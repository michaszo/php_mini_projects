<?php require 'classes/pumpes_controller.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ile_pompek</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Staatliches&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <main class="container mx-auto">
        <div class="row justify-content-center  align-items-center">
            <div class="card text-white bg-success mb-3" style="max-width: 18rem;">
                <div class="card-header"><h2>zrobiłeś już dziś:</h2></div>
                <div class="card-body">
                    <p class="card-text card-text_counter text-center"><?=$this_day_pumpes?></p>
                    <p class="card-text text-center">ogólnie: <?= $all_pumpes ?></p>
                    <form action="classes/add_pumps.php" method="post">
                        <button type="submit" class="btn btn-outline-warning btn-lg btn-block">dodaj 5 pompek!</button>
                    </form>
                </div>
            </div>
        </div>
    </main>
</body>
</html>
