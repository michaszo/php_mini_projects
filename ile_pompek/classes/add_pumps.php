<?php
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});


$database = new Database();
$db = $database->connect();
$pump = new pumps_m($db);
$pump->add_pumps(5);
header('Location: ../index.php');
