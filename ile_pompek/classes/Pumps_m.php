<?php
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

class pumps_m
{
    private $conn;
    private $table = 'pumps';

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function all_pumpes()
    {
        $query = 'SELECT SUM(counter) AS counter FROM ' . $this->table;
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row['counter'];
    }

    public function this_day_pumpes()
    {
        $date = Date::getDate();
        $query = 'SELECT counter FROM ' . $this->table . " WHERE  date  = '$date'";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return ($row['counter'] == 0) ? 0 : $row['counter'];
    }

    public function add_pumps(int $pumps)
    {
        $date = Date::getDate();
        $query = 'SELECT counter FROM ' . $this->table . " WHERE  date  = '$date'";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row != false) {
            $counter = $row['counter'] + $pumps;
            $query = "UPDATE " . $this->table . " SET counter = $counter WHERE date  = '$date'";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':date', $date);
            $stmt->bindParam(':pumps', $pumps);
        } else {
            $query = 'INSERT INTO ' . $this->table . ' SET date = :date, counter = :pumps';
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':date', $date);
            $stmt->bindParam(':pumps', $pumps);
        }
        if ($stmt->execute()) {
            return true;
        }
        echo "Error: $stmt->error. \n";
        return false;
    }
}
