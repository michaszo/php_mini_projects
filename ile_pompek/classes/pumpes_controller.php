<?php
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

$database = new Database();
$db = $database->connect();

$pumpes = new pumps_m($db);

$all_pumpes = $pumpes->all_pumpes();
$this_day_pumpes = $pumpes->this_day_pumpes();
