<?php
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

class Task
{
    private $conn;
    private $id;
    private $name;
    private $status;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function create($name): bool
    {
        $query = 'INSERT INTO tasks (name) VALUES (:name)';
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':name', $name);

        if ($stmt->execute()) {
            return true;
        }
        echo "Error: $stmt->error. \n";
        return false;
    }

    public function show()
    {
        $query = 'SELECT * FROM tasks';
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt->FetchAll(PDO::FETCH_ASSOC);
    }
}