<?php
spl_autoload_register(function ($class_name) {
    include './classes/' . $class_name . '.php';
});

$conn = new Database();
$conn = $conn->connect();

$task = new Task($conn);

$tasks = $task->show();

