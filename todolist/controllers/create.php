<?php
spl_autoload_register(function ($class_name) {
    include '../classes/' . $class_name . '.php';
});

session_start();

if (strlen($_POST['task']) >= 3) {
    $_SESSION['info'] = null;

    $conn = new Database();
    $conn = $conn->connect();

    $task = new Task($conn);

    $task->create($_POST['task']);

    header('Location: ../index.php');
} else {
    $_SESSION['info'] = 'Task must to be longer than 3 characters';
    header('Location: ../index.php');
}



