<?php require_once './templates/header.php'; ?>
    <div class="jumbotron text-center">
        <div class="row mb-4">
            <div class="col-sm-6 offset-md-3">
                <h1>ToDoList</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 offset-md-3 form-group">
                <form action="controllers/create.php" method="post">
                    <div class="row">
                        <div class="col-md-6 offset-md-1">
                            <input type="text" class="form-control" name="task" placeholder="Type your task!">
                        </div>
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary mb-2">Add task</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-6 offset-md-3">

                <?php if (isset($_SESSION['info'])) : ?>
                    <div class="alert alert-primary">
                        <strong>Info!</strong> <?= $_SESSION['info'] ?>
                    </div>
                <?php endif ?>

                <div class="list-group">
                    <?php require_once 'controllers/show.php';

                    foreach ($tasks as $task) {
                        echo '<a href="id_' .$task['id']. '" class="list-group-item list-group-item-action">' .$task['name']. '</a>';
                    }


                    ?>
                </div>
            </div>
        </div>
    </div>

<?php require_once 'templates/header.php'; ?>