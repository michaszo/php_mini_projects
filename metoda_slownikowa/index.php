<?php
$passwords = ['ania', 'burek', 'nowak'];

$password_to_guess = '!ania14';
$hp = password_hash($password_to_guess, PASSWORD_DEFAULT);

$start = microtime(true);
echo dictionary_method($passwords, $hp);
$stop = microtime(true);
echo '<br>' . bcsub($stop, $start, 4);

function dictionary_method($passwords, $hp)
{
    foreach ($passwords as $pass) {

        if (password_verify($pass, $hp)) {
            return $pass;
        }

        for ($i = 0; $i <= 10; $i++) {

            if (password_verify($pass . $i, $hp)) {
                return $pass . $i;
            }

            if (password_verify(ucfirst($pass . $i), $hp)) {
                return ucfirst($pass . $i);
            }

            if (password_verify('!' . $pass . $i, $hp)) {
                return '!' . $pass . $i;
            }

        }
    }

    return 0;
}

